// Fill out your copyright notice in the Description page of Project Settings.


#include "Timer.h"

#include "Kismet/KismetSystemLibrary.h"

// Sets default values for this component's properties
UTimer::UTimer()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTimer::BeginPlay()
{
	Super::BeginPlay();
	PrimaryComponentTick.TickInterval = 1;

	// ...
	CurrentTimeText = TEXT("00:00:00");

}


// Called every frame
void UTimer::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...

	CurrentTime += DeltaTime;

	const int IntTime = CurrentTime;
	CurrentTimeText = FString::Printf(TEXT("%02d:%02d:%02d"), IntTime / (60 * 60), (IntTime / 60) % 60, (IntTime) % 60);
	
	//UKismetSystemLibrary::PrintString(this, CurrentTimeText);
}

